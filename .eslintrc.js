module.exports = {
  "env": {
     "node": true,
     "mocha": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
      "sourceType": "module",
      "ecmaVersion": 2017
  },
  "plugins": ["mocha"],
  "globals": {
    "Promise"    : false
  },
  "rules": {
      "indent": [
          "error",
          2
      ],
      "no-console": 1,
      "linebreak-style": [
          "error",
          "unix"
      ],
      "quotes": [
          "error",
          "single"
      ],
      "semi": [
          "error",
          "always"
      ],
      "no-console": 0,
      "mocha/no-exclusive-tests": "error"
  }
};
