const omikujis = [
  '大吉',
  '吉',
  '中吉',
  '末吉',
  '凶',
  '大凶'
];

/**
 * ランダムな整数を生成する関数
 * @param {number} min 最小値
 * @param {number} max 最大値
 * @return {number} 結果 
 */
const getRandomInteger = (min, max) => {
  return Math.floor( Math.random() * (max + 1 - min) ) + min ;
}

/**
 * おみくじの結果を生成する関数
 * @return {string} 結果
 */
export const makeOmikuji = () => {
  const min = 0;
  const max = omikujis.length - 1;
  const seed = getRandomInteger(min, max);
  const result = omikujis[seed];
  return result;
};
