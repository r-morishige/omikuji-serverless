const timezoneOffset = 9 * 60 * 60 * 1000; //JST = UTC +9:00(millsecond)
export const jst = {
  getNowDate() {
    return new Date(Date.now() + timezoneOffset);
  },
  toUTC(jstDate) {
    return new Date(jstDate - timezoneOffset);
  }
};

export default jst;
