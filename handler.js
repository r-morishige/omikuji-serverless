import { makeOmikuji } from './src/makeOmikuji';
import { jst } from './src/jst';

import * as AWS from 'aws-sdk';
AWS.config.update({ region: 'ap-northeast-1' });

const stage = process.env.STAGE;
const db = new AWS.DynamoDB.DocumentClient();
const headers = { 'Access-Control-Allow-Origin': '*' };

/**
 * おみくじの結果をDynamoDBにPUTする関数
 * @param {string} user 
 * @param {string} result 
 */
const putOmikujiLog = async (user, omikuji) => {
  const currentDate = jst.getNowDate().toISOString();

  /* DynamoDBへ送信するパラメータ定義 */
  const params = {
    TableName: `omikujiLogTable_${stage}`,
    /* 送信するアイテム */
    Item: {
      user: user,
      date: currentDate,
      result: omikuji
    },
    /* 条件定義 */
    ExpressionAttributeNames: {
      '#date': 'date',
      '#user': 'user'
    },
    ConditionExpression: 'attribute_not_exists(#user) AND attribute_not_exists(#date)'
  };
  return await db.put(params).promise();
};

/**
 * getOmikujiの実行時に呼び出される関数
 * @param {any} event 
 * @param {any} context 
 * @param {function} callback 
 */
export const getOmikuji = async (event, context, callback) => {
  /* ユーザー名とおみくじ結果を定義 */
  const user = event.queryStringParameters.user;
  const omikuji = makeOmikuji();

  /* 非同期でDynamoDBに結果をPUTする */
  const dbResult = await putOmikujiLog(user, omikuji).catch(err => {
    /* 失敗時処理 */
    const message = '処理に失敗しました。';
    const body = JSON.stringify({ message });
    callback(null, { statusCode: 500, headers, body });
    return;
  });
  
  /* 成功したらおみくじ結果を返す */
  const message = `${user}の運勢は${omikuji}です。`;
  const body = JSON.stringify({ message });
  callback(null, { statusCode: 200, headers, body });
}